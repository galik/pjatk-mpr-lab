package mpr.domain;

/**
 *
 * @author s13128
 */
public class User extends Entity {

    private String username;
    private String password;

    private Person person;

    /**
     * @return String username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param String the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return String password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param String the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return mpr.domain.Person
     */
    public Person getPerson() {
        return this.person;
    }

    /**
     * @param Person person
     */
    public void setPerson(Person person) {
        this.person = person;
    }

}
