package mpr.service;

import mpr.domain.Person;
import mpr.domain.Role;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by galik on 22.01.2016.
 */
public class PersonService {

    public static List<Person> findPersonsWhoHaveMoreThanOneRole(List<Person> personList) {
        personList = personList.stream()
                .filter(p -> p.getRoles().size()>1)
                .collect(Collectors.toList());
        return personList;
    }

    public static String findOldestPerson(List<Person> personList) {
        Person oldest = personList.stream()
                .max(Comparator.comparing(Person::getAge))
                .get();
        return oldest.getPersonDetails();

    }

    public static Person findPersonWithLongestSurname(List<Person> personList) {
        Person longest = personList.stream()
                .max(Comparator.comparing((Person person) -> person.getSurname().length()))
                .get();
        return longest;
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllPersonsAbove18(List<Person> personList) {
        return personList.stream()
                .filter((Person p) -> p.getAge() > 18)
                .map((Person p) -> p.getFirstName()+" "+p.getSurname())
                .collect(Collectors.joining(","));
    }

    public static List<String> getSortedDescRoleOfPersonsWithNameStartingWithS(List<Person> personList) {
        List<List<Role>> listOfListsPermissions = personList.stream()
                .filter((Person p) -> p.getFirstName().substring(0, 1).equalsIgnoreCase("S"))
                .map((Person p) -> p.getRoles())
                .collect(Collectors.toList());

        List<Role> listOfPermissions =
                listOfListsPermissions.stream()
                        .flatMap(List::stream)
                        .collect(Collectors.toList());

        return listOfPermissions.stream()
                .map(Role::getRoleName)
                .distinct()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
    }

    public static void printLowerCasedRoleNamesOfPersonsWithSurnameEndingWithI(List<Person> personList) {
        List<List<Role>> listOfListsRoles = personList.stream()
                .filter((Person p) -> p.getSurname().substring(p.getSurname().length() - 1).equalsIgnoreCase("i"))
                .map((Person p) -> p.getRoles())
                .collect(Collectors.toList());
    //W jednym streamie
        List<Role> listOfPermissions =
                listOfListsRoles.stream()
                        .flatMap(List::stream)
                        .collect(Collectors.toList());

        listOfPermissions.stream()
                .map(Role::getRoleName)
                .map(String::toLowerCase)
                .distinct()
                .sorted()
                .forEach(System.out::println);
    }

    public static Map<Integer, List<Person>> groupPersonsByAdress(List<Person> personList) {
        Map<Integer, List<Person>> groupUsersByRole = personList.stream()
                .collect(Collectors.groupingBy((Person p) -> p.getAdress().getId()));
        return groupUsersByRole;
    }

    public static Map<Boolean, List<Person>> partitionPersonByUnderAndOver18(List<Person> personList) {
        Map<Boolean, List<Person>> partitionUsersByAge = personList.stream()
                .collect(Collectors.partitioningBy((Person p) -> p.getAge() >= 18));
        return partitionUsersByAge;
    }
}
