package mpr.tests;

import mpr.db.PersonRepository;
import mpr.db.Repository;
import mpr.db.repositories.SqLitePersonRepository;
import mpr.db.repositories.SqLiteRepository;
import mpr.db.repositories.retrievers.IEntityRetriever;
import mpr.db.unitofwork.IUnitOfWorkRepository;
import mpr.db.unitofwork.UnitOfWork;
import mpr.domain.Entity;
import mpr.domain.EntityState;
import mpr.domain.Person;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.*;

/**
 * Created by galik on 23.01.2016.
 */
public class PersonRepositoryTest {

    Repository<Person> personRepository;

    @Mock
    private Map<Entity, IUnitOfWorkRepository> entities;
    Connection connection;
    Person person;
    IEntityRetriever retriever;
    SqLiteRepository<Person> personRepositoryMock;

    @InjectMocks
    UnitOfWork uow;

    @Before
    public void initMocks() throws SQLException {
        uow = mock(UnitOfWork.class);
        MockitoAnnotations.initMocks(this);
        connection = mock(Connection.class);
        when(connection.createStatement()).thenReturn(mock(java.sql.Statement.class));
        when(connection.prepareStatement(anyString())).thenReturn(mock(PreparedStatement.class));
        personRepository = new SqLitePersonRepository(connection, retriever, uow);
        person = mock(Person.class);
        personRepositoryMock = mock(SqLiteRepository.class);
    }

    @Test(expected = NullPointerException.class)
    public void test_save_with_null_as_argument() throws SQLException {
        personRepository.add(null);
    }

    @Test
    public void test_save_with_obj_as_argument() throws SQLException {
        doCallRealMethod().when(person).setState((EntityState) any());
        doCallRealMethod().when(person).getState();
        doCallRealMethod().when(uow).markAsNew((Entity) any(), (IUnitOfWorkRepository) any());

        personRepository.add(person);

        assertSame(EntityState.New, person.getState());
    }

    @Test
    public void test_update_with_obj_as_argument() throws SQLException {
        doCallRealMethod().when(person).setState((EntityState) any());
        doCallRealMethod().when(person).getState();
        doCallRealMethod().when(uow).markAsNew((Entity) any() ,(IUnitOfWorkRepository) any());

        personRepository.add(person);
        doCallRealMethod().when(uow).markAsDirty((Entity) any(), (IUnitOfWorkRepository) any());
        personRepository.modify(person);

        assertSame(EntityState.Changed, person.getState());
    }

    @Test(expected = IllegalStateException.class)
    public void test_update_with_argument_which_is_not_in_database() throws SQLException {
        doCallRealMethod().when(person).setState((EntityState) any());
        doCallRealMethod().when(person).getState();
        doCallRealMethod().when(uow).markAsDirty((Entity) any(), (IUnitOfWorkRepository) any());

        personRepository.modify(person);
    }

    @Test(expected = NullPointerException.class)
    public void test_update_with_null_as_argument() throws SQLException {
        doCallRealMethod().when(uow).markAsDirty((Entity) any(), (IUnitOfWorkRepository) any());
        personRepository.modify(null);
    }

    @Test
    public void test_update_data_is_valid() throws SQLException {
        doCallRealMethod().when(person).setState((EntityState) any());
        doCallRealMethod().when(person).getState();
        doCallRealMethod().when(uow).markAsNew((Entity) any(), (IUnitOfWorkRepository) any());
        doCallRealMethod().when(personRepositoryMock).withId((Integer) any());
        when(personRepositoryMock.withId(anyInt())).thenReturn(person);
//        doCallRealMethod().when(uow).commit();
        person.setFirstName("Piotr");
        person.setSurname("TEST");
        person.setAge(25);
        person.setId(1);

        personRepositoryMock.add(person);
//        verify(personRepositoryMock, times(1)).persistAdd(person);
//        verify(personRepositoryMock, times(1)).add(person);
//        personRepositoryMock.
        uow.commit();
        assertSame(25, personRepositoryMock.withId(1).getAge());

    }
}
