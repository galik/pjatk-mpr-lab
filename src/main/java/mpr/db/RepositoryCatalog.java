package mpr.db;

import mpr.db.repositories.*;

/**
 * Created by galik on 22.01.2016.
 */
public interface RepositoryCatalog {


    public SqLiteAddressRepository address();

    public SqLitePermissionRepository permission();

    public SqLiteRoleRepository role();

    public SqLitePersonRepository person();

    public SqLiteRolePermissionRepository rolePermission();

    public SqLitePersonRoleRepository personRole();
}
