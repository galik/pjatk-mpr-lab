package mpr.db;

import mpr.domain.Address;

import java.util.List;

/**
 * Created by galik on 21.01.2016.
 */
public interface AddressRepository extends Repository<Address> {

    List<Address> withCountryName(String countryName, PagingInfo page);
    List<Address> withProvinceName(String provinceName, PagingInfo page);
    List<Address> withCityName(String cityName, PagingInfo page);
    List<Address> withStreetName(String streetName, PagingInfo page);
}
