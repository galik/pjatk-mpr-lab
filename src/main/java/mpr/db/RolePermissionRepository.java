package mpr.db;

import mpr.domain.RolePermission;

import java.util.List;

/**
 * Created by galik on 21.01.2016.
 */
public interface RolePermissionRepository extends Repository<RolePermission> {

    List<RolePermission> withRoleId(Integer roleId, PagingInfo page);
    List<RolePermission> withPermissionId(Integer permissionId, PagingInfo page);
}
