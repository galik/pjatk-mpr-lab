package mpr.db;

import mpr.domain.Person;

import java.util.List;

public interface Repository<TEntity> {

    public TEntity withId(int id);
    public List<TEntity> allOnPage(PagingInfo page);
    public void add(TEntity entity);
    void modify(TEntity entity);
    public void remove(TEntity entity);
}