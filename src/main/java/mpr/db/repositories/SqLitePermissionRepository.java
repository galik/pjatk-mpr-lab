package mpr.db.repositories;

import mpr.db.PagingInfo;
import mpr.db.repositories.retrievers.IEntityRetriever;
import mpr.db.unitofwork.IUnitOfWork;
import mpr.db.unitofwork.IUnitOfWorkRepository;
import mpr.domain.Entity;
import mpr.domain.EntityState;
import mpr.domain.Permission;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SqLitePermissionRepository extends SqLiteRepository implements IUnitOfWorkRepository {

    private Connection connection;

    private String insertSql = "INSERT INTO Permission(permissionLevel, permissionName) VALUES (?, ?)";
    private String selectSql = "SELECT * FROM Permission LIMIT ?, ?";
    private String selectByIdSql ="SELECT * FROM Permission WHERE id = ?";
    private String selectByPermissionLevelSql ="SELECT * FROM Permission WHERE permissionLevel = ? LIMIT ?, ?";
    private String selectByPermissionNameSql ="SELECT * FROM Permission WHERE permissionName = ? LIMIT ?, ?";
    private String deleteSql = "DELETE FROM Permission WHERE id = ?";
    private String updateSql = "UPDATE Permission SET permissionLevel = ?, permissionName = ? WHERE id = ?";


    private PreparedStatement insert;
    private PreparedStatement select;
    private PreparedStatement selectById;
    private PreparedStatement selectByPermissionLevel;
    private PreparedStatement selectByPermissionName;
    private PreparedStatement delete;
    private PreparedStatement update;

    private String createTablePermission = "CREATE TABLE Permission(" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "permissionLevel INT," +
            "permissionName VARCHAR(20))";

    public SqLitePermissionRepository(Connection connection, IEntityRetriever<Permission> builder, IUnitOfWork uow) {
        super(connection, builder, uow);
        try {
            selectById = connection.prepareStatement(selectByIdSql);
            selectByPermissionName=connection.prepareStatement(selectByPermissionNameSql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Permission> withPermissionLevel(Integer permissionLevel, PagingInfo page) {
        List<Permission> result = new ArrayList<Permission>();
        try {
            selectByPermissionLevel.setInt(1, permissionLevel);
            selectByPermissionLevel.setInt(2, page.getCurrentPage() * page.getSize());
            selectByPermissionLevel.setInt(3, page.getSize());
            ResultSet rs = selectByPermissionLevel.executeQuery();
            while(rs.next()){
                Permission permission = new Permission();
                permission.setPermissionLevel(rs.getInt("permissionLevel"));
                permission.setPermissionName(rs.getString("permissionName"));
                permission.setId(rs.getInt("id"));
                result.add(permission);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<Permission> withPermissionName(String permissionName, PagingInfo page) {
        List<Permission> result = new ArrayList<Permission>();
        try {
            selectByPermissionName.setString(1, permissionName);
            selectByPermissionName.setInt(2, page.getCurrentPage() * page.getSize());
            selectByPermissionName.setInt(3, page.getSize());
            ResultSet rs = selectByPermissionName.executeQuery();
            while(rs.next()){
                Permission permission = new Permission();
                permission.setPermissionLevel(rs.getInt("permissionLevel"));
                permission.setPermissionName(rs.getString("permissionName"));
                permission.setId(rs.getInt("id"));
                result.add(permission);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Permission withId(int id) {
        Permission result = null;
        try {
            selectById.setInt(1, id);
            ResultSet rs = selectById.executeQuery();
            while(rs.next()){
                Permission permission = new Permission();
                permission.setPermissionLevel(rs.getInt("permissionLevel"));
                permission.setPermissionName(rs.getString("permissionName"));
                permission.setId(rs.getInt("id"));
                result = permission;
                break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<Permission> allOnPage(PagingInfo page) {
        List<Permission> result = new ArrayList<Permission>();

        try {
            select.setInt(1, page.getCurrentPage() * page.getSize());
            select.setInt(2, page.getSize());
            ResultSet rs = select.executeQuery();
            while(rs.next()){
                Permission permission = new Permission();
                permission.setPermissionLevel(rs.getInt("permissionLevel"));
                permission.setPermissionName(rs.getString("permissionName"));
                permission.setId(rs.getInt("id"));
                result.add(permission);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void add(Object o) {
    }

    @Override
    public void modify(Object o) {

    }

    @Override
    public void remove(Object o) {

    }

    public Integer add(Permission permission) {
        Integer id = 0;
        try {
            insert.setInt(1, permission.getPermissionLevel());
            insert.setString(2, permission.getPermissionName());
            insert.executeUpdate();
            ResultSet rs = insert.getGeneratedKeys();
            if (rs.next()){
                id = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    public void modify(Permission permission) {
        try {
            update.setInt(1, permission.getPermissionLevel());
            update.setString(2, permission.getPermissionName());
            update.setInt(3, permission.getId());
            update.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void remove(Permission permission) {
        try {
            delete.setInt(1, permission.getId());
            delete.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void save(Permission permission) {

    }

    public void persistAdd(Entity entity) {
        if(entity.getState() == EntityState.New){
            add((Permission) entity);
        }
    }

    public void persistUpdate(Entity entity) {
        if(entity.getState() == EntityState.Changed){
            modify((Permission) entity);
        }
    }

    public void persistDelete(Entity entity) {
        if(entity.getState() == EntityState.Deleted){
            remove((Permission) entity);
        }
    }

    @Override
    protected void setUpUpdateQuery(Entity entity) throws SQLException {

    }

    @Override
    protected void setUpInsertQuery(Entity entity) throws SQLException {

    }

    @Override
    protected String getCreateTable() {
        return null;
    }

    @Override
    protected String getTableName() {
        return null;
    }

    @Override
    protected String getUpdateQuery() {
        return null;
    }

    @Override
    protected String getInsertQuery() {
        return null;
    }

    @Override
    public void remove(Entity entity) {

    }
}