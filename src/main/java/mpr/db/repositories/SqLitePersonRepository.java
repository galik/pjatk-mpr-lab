package mpr.db.repositories;

import mpr.db.PagingInfo;
import mpr.db.Repository;
import mpr.db.repositories.retrievers.IEntityRetriever;
import mpr.db.unitofwork.IUnitOfWork;
import mpr.db.unitofwork.IUnitOfWorkRepository;
import mpr.domain.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SqLitePersonRepository extends SqLiteRepository<Person> {

    private Connection connection;

    private String insertSql = "INSERT INTO Person(firstName, surname, addressId, age) VALUES (?, ?, ?, ?)";
    private String selectSql = "SELECT * FROM Person LIMIT ?, ?";
    private String selectByIdSql ="SELECT * FROM Person WHERE id = ?";
    private String selectByFirstNameSql ="SELECT * FROM Person WHERE firstName = ? LIMIT ?, ?";
    private String selectBySurnameSql ="SELECT * FROM Person WHERE surname = ? LIMIT ?, ?";
    private String selectByAddressIdSql ="SELECT * FROM Person WHERE addressId = ? LIMIT ?, ?";
    private String deleteSql = "DELETE FROM Person WHERE id = ?";
    private String updateSql = "UPDATE Person SET firstName = ?, surname = ?, addressId = ?, age = ? WHERE id = ?";


    private PreparedStatement insert;
    private PreparedStatement select;
    private PreparedStatement selectById;
    private PreparedStatement selectByFirstName;
    private PreparedStatement selectBySurname;
    private PreparedStatement selectByAddressId;
    private PreparedStatement delete;
    private PreparedStatement update;

    private String createTablePerson = "CREATE TABLE Person(" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "firstName VARCHAR(20)," +
            "surname VARCHAR(20)," +
            "addressId INT," +
            "age INT)";

    public SqLitePersonRepository(Connection connection, IEntityRetriever<Person> builder, IUnitOfWork uow) {
        super(connection, builder, uow);
        try {
            selectById = connection.prepareStatement(selectByIdSql);
            selectByFirstName=connection.prepareStatement(selectByFirstNameSql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Person> withFirstName(String firstName, PagingInfo page) {
        List<Person> result = new ArrayList<Person>();
//        SqLiteAddressRepository addressRepository = new SqLiteAddressRepository(this.connection);
//        SqLitePersonRoleRepository personRoleRepository = new SqLitePersonRoleRepository(this.connection);
//        SqLiteRoleRepository roleRepository = new SqLiteRoleRepository(this.connection);
//        try {
//            selectByFirstName.setString(1, firstName);
//            selectByFirstName.setInt(2, page.getCurrentPage() * page.getSize());
//            selectByFirstName.setInt(3, page.getSize());
//            ResultSet rs = selectByFirstName.executeQuery();
//            while(rs.next()){
//                Person person = new Person();
//                person.setFirstName(rs.getString("firstName"));
//                person.setSurname(rs.getString("surname"));
//                person.addAdress(addressRepository.withId(rs.getInt("addressId")));
//                person.setAge(rs.getInt("age"));
//                person.setId(rs.getInt("id"));
//                List<PersonRole> personRoles = personRoleRepository.withPersonId(person.getId(), new PagingInfo());
//
//                for (PersonRole pr : personRoles) {
//                    Role r = roleRepository.withId(pr.getId());
//                    person.addRoles(r);
//                }
//                result.add(person);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
        return result;
    }

    public List<Person> withSurname(String surname, PagingInfo page) {
        List<Person> result = new ArrayList<Person>();
//        SqLiteAddressRepository addressRepository = new SqLiteAddressRepository(this.connection);
//        try {
//            selectBySurname.setString(1, surname);
//            selectBySurname.setInt(2, page.getCurrentPage() * page.getSize());
//            selectBySurname.setInt(3, page.getSize());
//            ResultSet rs = selectBySurname.executeQuery();
//            while(rs.next()){
//                Person person = new Person();
//                person.setFirstName(rs.getString("firstName"));
//                person.setSurname(rs.getString("surname"));
//                person.addAdress(addressRepository.withId(rs.getInt("addressId")));
//                person.setAge(rs.getInt("age"));
//                person.setId(rs.getInt("id"));
//                result.add(person);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
        return result;
    }

    public List<Person> withAddressId(Integer addressId, PagingInfo page) {
        List<Person> result = new ArrayList<Person>();
//        SqLiteAddressRepository addressRepository = new SqLiteAddressRepository(this.connection);
//        try {
//            selectByAddressId.setInt(1, addressId);
//            selectByAddressId.setInt(2, page.getCurrentPage() * page.getSize());
//            selectByAddressId.setInt(3, page.getSize());
//            ResultSet rs = selectByAddressId.executeQuery();
//            while(rs.next()){
//                Person person = new Person();
//                person.setFirstName(rs.getString("firstName"));
//                person.setSurname(rs.getString("surname"));
//                person.addAdress(addressRepository.withId(addressId));
//                person.setAge(rs.getInt("age"));
//                person.setId(rs.getInt("id"));
//                result.add(person);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
        return result;
    }

    public Person withId(int id) {
        Person result = null;
//        SqLiteAddressRepository addressRepository = new SqLiteAddressRepository(this.connection);
//        SqLitePersonRoleRepository personRoleRepository = new SqLitePersonRoleRepository(this.connection);
//        SqLiteRoleRepository roleRepository = new SqLiteRoleRepository(this.connection);
//        try {
//            selectById.setInt(1, id);
//            ResultSet rs = selectById.executeQuery();
//            while(rs.next()){
//                Person person = new Person();
//                person.setFirstName(rs.getString("firstName"));
//                person.setSurname(rs.getString("surname"));
//                person.addAdress(addressRepository.withId(rs.getInt("addressId")));
//                person.setAge(rs.getInt("age"));
//                PagingInfo pInfo = new PagingInfo();
//                pInfo.setSize(1);
//                List<PersonRole> personRoles = personRoleRepository.withPersonId(id, pInfo);
//                for (PersonRole pr : personRoles) {
//                    person.addRoles(roleRepository.withId(pr.getRoleId()));
//                }
//                person.setId(rs.getInt("id"));
//                result = person;
//                break;
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
        return result;
    }

    public List<Person> allOnPage(PagingInfo page) {
        List<Person> result = new ArrayList<Person>();
//        SqLiteAddressRepository addressRepository = new SqLiteAddressRepository(this.connection);
//        SqLitePersonRoleRepository personRoleRepository = new SqLitePersonRoleRepository(this.connection);
//        SqLiteRoleRepository roleRepository = new SqLiteRoleRepository(this.connection);
//
//        try {
//            select.setInt(1, page.getCurrentPage() * page.getSize());
//            select.setInt(2, page.getSize());
//            ResultSet rs = select.executeQuery();
//            while(rs.next()){
//                Person person = new Person();
//                person.setFirstName(rs.getString("firstName"));
//                person.setSurname(rs.getString("surname"));
//                person.addAdress(addressRepository.withId(rs.getInt("addressId")));
//                person.setId(rs.getInt("id"));
//                PagingInfo pInfo = new PagingInfo();
//                pInfo.setSize(10);
//                List<PersonRole> personRoles = personRoleRepository.withPersonId(person.getId(), pInfo);
//                for (PersonRole pr : personRoles) {
//                    person.addRoles(roleRepository.withId(pr.getRoleId()));
//                }
//                person.setAge(rs.getInt("age"));
//                result.add(person);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
        return result;
    }

//    public Integer add(Object o) {
//        return null;
//    }


//    public void add(Person person) {
//        Integer id = 0;
//        SqLiteAddressRepository addressRepository = new SqLiteAddressRepository(this.connection);
//        try {
//            insert.setString(1, person.getFirstName());
//            insert.setString(2, person.getSurname());
////            Integer addressId = addressRepository.add(person.getAdress());
//            insert.setInt(3, 1);
//            insert.setInt(4, person.getAge());
//            insert.executeUpdate();
//            ResultSet rs = insert.getGeneratedKeys();
//            if (rs.next()){
//                id = rs.getInt(1);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return id;
//    }

//    public void modify(Person person) {
//
//    }

    @Override
    public void remove(Person person) {
        try {
            delete.setInt(1, person.getId());
            delete.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void persistAdd(Entity entity) {
        if(entity.getState() == EntityState.New){
            add((Person) entity);
        }
    }

    public void persistUpdate(Entity entity) {
        if(entity.getState() == EntityState.Changed){
            modify((Person) entity);
        }
    }

    public void persistDelete(Entity entity) {
        if(entity.getState() == EntityState.Deleted){
            remove((Person) entity);
        }
    }

    @Override
    protected void setUpUpdateQuery(Person person) throws SQLException {
        update.setString(1, person.getFirstName());
        update.setString(2, person.getSurname());
        update.setInt(3, person.getAdress().getId());
        update.setInt(4, person.getAge());
        update.setInt(5, person.getId());
    }

    @Override
    protected void setUpInsertQuery(Person person) throws SQLException {
//        SqLiteAddressRepository addressRepository = new SqLiteAddressRepository(this.connection);
//        try {
//            insert.setString(1, person.getFirstName());
//            insert.setString(2, person.getSurname());
////            Integer addressId = addressRepository.add(person.getAdress());
//            insert.setInt(3, 1);
//            insert.setInt(4, person.getAge());
//            insert.executeUpdate();
//            ResultSet rs = insert.getGeneratedKeys();
//            if (rs.next()){
//                id = rs.getInt(1);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    protected String getCreateTable() {
        return null;
    }

    @Override
    protected String getTableName() {
        return null;
    }

    @Override
    protected String getUpdateQuery() {
        return null;
    }

    @Override
    protected String getInsertQuery() {
        return null;
    }

//    @Override
//    public void remove(Entity entity) {
//
//    }
}