package mpr.db.repositories;

import mpr.db.Repository;
import mpr.db.repositories.retrievers.IEntityRetriever;
import mpr.db.unitofwork.IUnitOfWork;
import mpr.db.unitofwork.IUnitOfWorkRepository;
import mpr.domain.Entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by galik on 23.01.2016.
 */
public abstract class SqLiteRepository<TEntity extends Entity>
        implements Repository<TEntity>, IUnitOfWorkRepository {

    protected IUnitOfWork uow;
    protected Connection connection;
    protected PreparedStatement selectByID;
    protected PreparedStatement insert;
    protected PreparedStatement delete;
    protected PreparedStatement update;
    protected PreparedStatement selectAll;
    protected IEntityRetriever<TEntity> retriever;

    protected String selectByIDSql=
            "SELECT * FROM "
                    + getTableName()
                    + " WHERE id=?";
    protected String deleteSql=
            "DELETE FROM "
                    + getTableName()
                    + " WHERE id=?";
    protected String selectAllSql=
            "SELECT * FROM " + getTableName();

    protected SqLiteRepository(Connection connection,
                         IEntityRetriever<TEntity> retriever, IUnitOfWork uow) {
        this.uow = uow;
        this.retriever = retriever;
        this.connection = connection;
        try {
            Statement createTable = connection.createStatement();
            createTable.executeUpdate(getCreateTable());

            selectByID = connection.prepareStatement(selectByIDSql);
            insert = connection.prepareStatement(getInsertQuery());
            delete = connection.prepareStatement(deleteSql);
            update = connection.prepareStatement(getUpdateQuery());
            selectAll = connection.prepareStatement(selectAllSql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//    public abstract void remove(Person person);

    protected abstract void setUpUpdateQuery(TEntity entity) throws SQLException;
    protected abstract void setUpInsertQuery(TEntity entity) throws SQLException;
    protected abstract String getCreateTable();
    protected abstract String getTableName();
    protected abstract String getUpdateQuery();
    protected abstract String getInsertQuery();
    public abstract void remove(TEntity entity);

    public void add (TEntity entity) {
        checkNullPointerException(entity);
        uow.markAsNew(entity, this);
    }

    public void modify (TEntity entity) {
        checkNullPointerException(entity);
        if(entity.getState() == null) {
            throw new IllegalStateException();
        } else {
            uow.markAsDirty(entity, this);
        }
    }

    private void checkNullPointerException (Entity entity) {
        if (entity == null) {
            throw new NullPointerException();
        }
    }
}
