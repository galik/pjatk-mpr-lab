package mpr.db.repositories;

import mpr.db.PagingInfo;
import mpr.db.repositories.retrievers.IEntityRetriever;
import mpr.db.unitofwork.IUnitOfWork;
import mpr.db.unitofwork.IUnitOfWorkRepository;
import mpr.domain.Entity;
import mpr.domain.EntityState;
import mpr.domain.PersonRole;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SqLitePersonRoleRepository extends SqLiteRepository implements IUnitOfWorkRepository {

    private Connection connection;

    private String insertSql = "INSERT INTO PersonRole (personId, roleId) VALUES (?, ?)";
    private String selectSql = "SELECT * FROM PersonRole LIMIT ?, ?";
    private String selectByIdSql ="SELECT * FROM PersonRole WHERE id = ?";
    private String selectByPersonIdSql ="SELECT * FROM PersonRole WHERE personId = ? LIMIT ?, ?";
    private String selectByRoleIdSql ="SELECT * FROM PersonRole WHERE roleId = ? LIMIT ?, ?";
    private String deleteSql = "DELETE FROM PersonRole WHERE id = ?";
    private String updateSql = "UPDATE PersonRole SET personId = ?, roleId = ? WHERE id = ?";


    private PreparedStatement insert;
    private PreparedStatement select;
    private PreparedStatement selectById;
    private PreparedStatement selectByPersonId;
    private PreparedStatement selectByRoleId;
    private PreparedStatement delete;
    private PreparedStatement update;

    private String createTablePersonRole = "CREATE TABLE PersonRole(" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "personId INT," +
            "roleId INT)";

    public SqLitePersonRoleRepository(Connection connection, IEntityRetriever<PersonRole> builder, IUnitOfWork uow) {
        super(connection, builder, uow);
        try {
            selectById = connection.prepareStatement(selectByIdSql);
            selectByPersonId = connection.prepareStatement(selectByPersonIdSql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<PersonRole> withPersonId(Integer personId, PagingInfo page) {
        List<PersonRole> result = new ArrayList<PersonRole>();
        try {
            selectByPersonId.setInt(1, personId);
            selectByPersonId.setInt(2, page.getCurrentPage() * page.getSize());
            selectByPersonId.setInt(3, page.getSize());
            ResultSet rs = selectByPersonId.executeQuery();
            while(rs.next()){
                PersonRole personRole = new PersonRole();
                personRole.setPersonId(rs.getInt("personId"));
                personRole.setRoleId(rs.getInt("roleId"));
                personRole.setId(rs.getInt("id"));
                result.add(personRole);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<PersonRole> withRoleId(Integer roleId, PagingInfo page) {
        List<PersonRole> result = new ArrayList<PersonRole>();
        try {
            selectByRoleId.setInt(1, roleId);
            selectByRoleId.setInt(2, page.getCurrentPage() * page.getSize());
            selectByRoleId.setInt(3, page.getSize());
            ResultSet rs = selectByRoleId.executeQuery();
            while(rs.next()){
                PersonRole rolePermission = new PersonRole();
                rolePermission.setPersonId(rs.getInt("personId"));
                rolePermission.setRoleId(rs.getInt("roleId"));
                rolePermission.setId(rs.getInt("id"));
                result.add(rolePermission);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public PersonRole withId(int id) {
        PersonRole result = null;
        try {
            selectById.setInt(1, id);
            ResultSet rs = selectById.executeQuery();
            while(rs.next()){
                PersonRole rolePermission = new PersonRole();
                rolePermission.setPersonId(rs.getInt("personId"));
                rolePermission.setRoleId(rs.getInt("roleId"));
                rolePermission.setId(rs.getInt("id"));
                result = rolePermission;
                break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<PersonRole> allOnPage(PagingInfo page) {
        List<PersonRole> result = new ArrayList<PersonRole>();

        try {
            select.setInt(1, page.getCurrentPage() * page.getSize());
            select.setInt(2, page.getSize());
            ResultSet rs = select.executeQuery();
            while(rs.next()){
                PersonRole rolePermission = new PersonRole();
                rolePermission.setPersonId(rs.getInt("personId"));
                rolePermission.setRoleId(rs.getInt("roleId"));
                rolePermission.setId(rs.getInt("id"));
                result.add(rolePermission);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void add(Object o) {
    }

    @Override
    public void modify(Object o) {

    }

    @Override
    public void remove(Object o) {

    }

    public void add(PersonRole personRole) {
        Integer id = 0;
        try {
            insert.setInt(1, personRole.getPersonId());
            insert.setInt(2, personRole.getRoleId());
            insert.executeUpdate();
            ResultSet rs = insert.getGeneratedKeys();
            if (rs.next()){
                id = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        return id;
    }

    public void modify(PersonRole rolePermission) {
        try {
            update.setInt(1, rolePermission.getPersonId());
            update.setInt(2, rolePermission.getRoleId());
            update.setInt(3, rolePermission.getId());
            update.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void remove(PersonRole rolePermission) {
        try {
            delete.setInt(1, rolePermission.getId());
            delete.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void persistAdd(Entity entity) {
        if(entity.getState() == EntityState.New){
            add((PersonRole) entity);
        }
    }

    public void persistUpdate(Entity entity) {
        if(entity.getState() == EntityState.Changed){
            modify((PersonRole) entity);
        }
    }

    public void persistDelete(Entity entity) {
        if(entity.getState() == EntityState.Deleted){
            remove((PersonRole) entity);
        }
    }

    @Override
    protected void setUpUpdateQuery(Entity entity) throws SQLException {

    }

    @Override
    protected void setUpInsertQuery(Entity entity) throws SQLException {

    }

    @Override
    protected String getCreateTable() {
        return null;
    }

    @Override
    protected String getTableName() {
        return null;
    }

    @Override
    protected String getUpdateQuery() {
        return null;
    }

    @Override
    protected String getInsertQuery() {
        return null;
    }

    @Override
    public void remove(Entity entity) {

    }
}