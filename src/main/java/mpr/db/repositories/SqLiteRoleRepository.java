package mpr.db.repositories;

import mpr.db.PagingInfo;
import mpr.db.repositories.retrievers.IEntityRetriever;
import mpr.db.unitofwork.IUnitOfWork;
import mpr.db.unitofwork.IUnitOfWorkRepository;
import mpr.domain.Entity;
import mpr.domain.EntityState;
import mpr.domain.Role;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SqLiteRoleRepository extends SqLiteRepository implements IUnitOfWorkRepository {

    private Connection connection;

    private String insertSql = "INSERT INTO Role(roleName) VALUES (?)";
    private String selectSql = "SELECT * FROM Role LIMIT ?, ?";
    private String selectByIdSql ="SELECT * FROM Role WHERE id = ?";
    private String selectByRoleNameSql ="SELECT * FROM Role WHERE roleName = ? LIMIT ?, ?";
    private String deleteSql = "DELETE FROM Role WHERE id = ?";
    private String updateSql = "UPDATE Role SET roleName = ? WHERE id = ?";


    private PreparedStatement insert;
    private PreparedStatement select;
    private PreparedStatement selectById;
    private PreparedStatement selectByRoleName;
    private PreparedStatement delete;
    private PreparedStatement update;

    private String createTableRole = "CREATE TABLE Role(" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "roleName VARCHAR(20))";

    public SqLiteRoleRepository(Connection connection, IEntityRetriever<Role> builder, IUnitOfWork uow) {
        super(connection, builder, uow);
        try {
            selectById = connection.prepareStatement(selectByIdSql);
            selectByRoleName=connection.prepareStatement(selectByRoleNameSql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Role> withRoleName(String roleName, PagingInfo page) {
        List<Role> result = new ArrayList<Role>();
        try {
            selectByRoleName.setString(1, roleName);
            selectByRoleName.setInt(2, page.getCurrentPage() * page.getSize());
            selectByRoleName.setInt(3, page.getSize());
            ResultSet rs = selectByRoleName.executeQuery();
            while(rs.next()){
                Role role = new Role();
                role.setRoleName(rs.getString("roleName"));
                role.setId(rs.getInt("id"));
                result.add(role);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Role withId(int id) {
        Role result = null;
        try {
            selectById.setInt(1, id);
            ResultSet rs = selectById.executeQuery();
            while(rs.next()){
                Role role = new Role();
                role.setRoleName(rs.getString("roleName"));
                role.setId(rs.getInt("id"));
                result = role;
                break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<Role> allOnPage(PagingInfo page) {
        List<Role> result = new ArrayList<Role>();

        try {
            select.setInt(1, page.getCurrentPage() * page.getSize());
            select.setInt(2, page.getSize());
            ResultSet rs = select.executeQuery();
            while(rs.next()){
                Role role = new Role();
                role.setRoleName(rs.getString("roleName"));
                role.setId(rs.getInt("id"));
                result.add(role);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void add(Object o) {
    }

    @Override
    public void modify(Object o) {

    }

    @Override
    public void remove(Object o) {

    }

    public Integer add(Role role) {
        Integer id = 0;
        try {
            insert.setString(1, role.getRoleName());
            insert.executeUpdate();
            ResultSet rs = insert.getGeneratedKeys();
            if (rs.next()){
                id = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    public void modify(Role role) {
        try {
            update.setString(1, role.getRoleName());
            update.setInt(2, role.getId());
            update.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void remove(Role role) {
        try {
            delete.setInt(1, role.getId());
            delete.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void persistAdd(Entity entity) {
        if(entity.getState() == EntityState.New){
            add((Role) entity);
        }
    }

    public void persistUpdate(Entity entity) {
        if(entity.getState() == EntityState.Changed){
            modify((Role) entity);
        }
    }

    public void persistDelete(Entity entity) {
        if(entity.getState() == EntityState.Deleted){
            remove((Role) entity);
        }
    }

    @Override
    protected void setUpUpdateQuery(Entity entity) throws SQLException {

    }

    @Override
    protected void setUpInsertQuery(Entity entity) throws SQLException {

    }

    @Override
    protected String getCreateTable() {
        return null;
    }

    @Override
    protected String getTableName() {
        return null;
    }

    @Override
    protected String getUpdateQuery() {
        return null;
    }

    @Override
    protected String getInsertQuery() {
        return null;
    }

    @Override
    public void remove(Entity entity) {

    }
}