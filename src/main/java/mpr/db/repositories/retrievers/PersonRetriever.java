package mpr.db.repositories.retrievers;

import mpr.domain.Person;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by galik on 23.01.2016.
 */
public class PersonRetriever implements IEntityRetriever<Person> {

    @Override
    public Person build(ResultSet rs) throws SQLException {
        Person result = new Person();
        result.setId(rs.getInt("id"));
        result.setFirstName(rs.getString("firstName"));
        result.setSurname(rs.getString("surname"));
        result.setAge(rs.getInt("age"));
        return result;
    }
}
