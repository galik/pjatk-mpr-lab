package mpr.db.repositories.retrievers;

import mpr.domain.Entity;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by galik on 23.01.2016.
 */
public interface IEntityRetriever<TEntity extends Entity> {

    public TEntity build(ResultSet rs) throws SQLException;

}
