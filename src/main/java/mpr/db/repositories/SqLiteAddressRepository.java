package mpr.db.repositories;

import mpr.db.PagingInfo;
import mpr.db.repositories.retrievers.IEntityRetriever;
import mpr.db.unitofwork.IUnitOfWork;
import mpr.db.unitofwork.IUnitOfWorkRepository;
import mpr.domain.Address;
import mpr.domain.Entity;
import mpr.domain.EntityState;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SqLiteAddressRepository extends SqLiteRepository implements IUnitOfWorkRepository {

    private Connection connection;

    private String insertSql = "INSERT INTO Address(streetName, cityName, provinceName, countryName) VALUES (?, ?, ?, ?)";
    private String selectSql = "SELECT * FROM Address LIMIT ?, ?";
    private String selectByIdSql ="SELECT * FROM Address WHERE id = ?";
    private String selectByStreetNameSql ="SELECT * FROM Address WHERE streetName = ? LIMIT ?, ?";
    private String selectByCityNameSql ="SELECT * FROM Address WHERE cityName = ? LIMIT ?, ?";
    private String selectByProvinceNameSql ="SELECT * FROM Address WHERE provinceName = ? LIMIT ?, ?";
    private String selectByCountryNameSql ="SELECT * FROM Address WHERE countryName = ? LIMIT ?, ?";
    private String deleteSql = "DELETE FROM Address WHERE id = ?";
    private String updateSql = "UPDATE Address SET streetName = ?, cityName = ?, provinceName = ?, countryName = ? WHERE id = ?";


    private PreparedStatement insert;
    private PreparedStatement select;
    private PreparedStatement selectById;
    private PreparedStatement selectByStreetName;
    private PreparedStatement selectByCityName;
    private PreparedStatement selectByProvinceName;
    private PreparedStatement selectByCountryName;
    private PreparedStatement delete;
    private PreparedStatement update;

    private String createTableAddress = "" +
            "CREATE TABLE Address(" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "countryName VARCHAR(20)," +
            "provinceName VARCHAR(20)," +
            "cityName VARCHAR(20)," +
            "streetName VARCHAR(20))";

    public SqLiteAddressRepository(Connection connection, IEntityRetriever<Address> builder, IUnitOfWork uow) {
        super(connection, builder, uow);
        try {
            selectById = connection.prepareStatement(selectByIdSql);
            selectByStreetName=connection.prepareStatement(selectByStreetNameSql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Address withId(int id) {
        Address result = null;
        try {
            selectById.setInt(1, id);
            ResultSet rs = selectById.executeQuery();
            while(rs.next()){
                Address address = new Address();
                address.setStreetName(rs.getString("streetName"));
                address.setCityName(rs.getString("cityName"));
                address.setProvinceName(rs.getString("provinceName"));
                address.setCountryName(rs.getString("countryName"));
                address.setId(rs.getInt("id"));
                result = address;
                break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<Address> allOnPage(PagingInfo page) {
        List<Address> result = new ArrayList<Address>();

        try {
            select.setInt(1, page.getCurrentPage() * page.getSize());
            select.setInt(2, page.getSize());
            ResultSet rs = select.executeQuery();
            while(rs.next()){
                Address address = new Address();
                address.setStreetName(rs.getString("streetName"));
                address.setCityName(rs.getString("cityName"));
                address.setProvinceName(rs.getString("provinceName"));
                address.setCountryName(rs.getString("countryName"));
                address.setId(rs.getInt("id"));
                result.add(address);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void add(Object o) {
    }

    public void modify(Object o) {

    }

    public void remove(Object o) {

    }

    public Integer add(Address address) {
        Integer id = 0;
        try {
            insert.setString(1, address.getStreetName());
            insert.setString(2, address.getCityName());
            insert.setString(3, address.getProvinceName());
            insert.setString(4, address.getCountryName());
            insert.executeUpdate();
            ResultSet rs = insert.getGeneratedKeys();
            if (rs.next()){
                id = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    public void modify(Address address) {
        try {
            update.setString(1, address.getStreetName());
            update.setString(2, address.getCityName());
            update.setString(3, address.getProvinceName());
            update.setString(4, address.getCountryName());
            update.setInt(5, address.getId());
            update.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void remove(Address address) {
        try {
            delete.setInt(1, address.getId());
            delete.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Address> withCountryName(String countryName, PagingInfo page) {
        List<Address> result = new ArrayList<Address>();
        try {
            selectByCountryName.setString(1, countryName);
            selectByCountryName.setInt(2, page.getCurrentPage() * page.getSize());
            selectByCountryName.setInt(3, page.getSize());
            ResultSet rs = selectByCountryName.executeQuery();
            while(rs.next()){
                Address address = new Address();
                address.setStreetName(rs.getString("streetName"));
                address.setCityName(rs.getString("cityName"));
                address.setProvinceName(rs.getString("provinceName"));
                address.setCountryName(rs.getString("countryName"));
                address.setId(rs.getInt("id"));
                result.add(address);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<Address> withProvinceName(String provinceName, PagingInfo page) {
        List<Address> result = new ArrayList<Address>();
        try {
            selectByProvinceName.setString(1, provinceName);
            selectByProvinceName.setInt(2, page.getCurrentPage() * page.getSize());
            selectByProvinceName.setInt(3, page.getSize());
            ResultSet rs = selectByProvinceName.executeQuery();
            while(rs.next()){
                Address address = new Address();
                address.setStreetName(rs.getString("streetName"));
                address.setCityName(rs.getString("cityName"));
                address.setProvinceName(rs.getString("provinceName"));
                address.setCountryName(rs.getString("countryName"));
                address.setId(rs.getInt("id"));
                result.add(address);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<Address> withCityName(String cityName, PagingInfo page) {
        List<Address> result = new ArrayList<Address>();
        try {
            selectByCityName.setString(1, cityName);
            selectByCityName.setInt(2, page.getCurrentPage() * page.getSize());
            selectByCityName.setInt(3, page.getSize());
            ResultSet rs = selectByCityName.executeQuery();
            while(rs.next()){
                Address address = new Address();
                address.setStreetName(rs.getString("streetName"));
                address.setCityName(rs.getString("cityName"));
                address.setProvinceName(rs.getString("provinceName"));
                address.setCountryName(rs.getString("countryName"));
                address.setId(rs.getInt("id"));
                result.add(address);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<Address> withStreetName(String streetName, PagingInfo page) {
        List<Address> result = new ArrayList<Address>();
        try {
            selectByStreetName.setString(1, streetName);
            selectByStreetName.setInt(2, page.getCurrentPage() * page.getSize());
            selectByStreetName.setInt(3, page.getSize());
            ResultSet rs = selectByStreetName.executeQuery();
            while(rs.next()){
                Address address = new Address();
                address.setStreetName(rs.getString("streetName"));
                address.setCityName(rs.getString("cityName"));
                address.setProvinceName(rs.getString("provinceName"));
                address.setCountryName(rs.getString("countryName"));
                address.setId(rs.getInt("id"));
                result.add(address);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void persistAdd(Entity entity) {
        if(entity.getState() == EntityState.New){
            add((Address) entity);
        }
    }

    public void persistUpdate(Entity entity) {
        if(entity.getState() == EntityState.Changed){
            modify((Address) entity);
        }
    }

    public void persistDelete(Entity entity) {
        if(entity.getState() == EntityState.Deleted){
            remove((Address) entity);
        }
    }

    @Override
    protected void setUpUpdateQuery(Entity entity) throws SQLException {

    }

    @Override
    protected void setUpInsertQuery(Entity entity) throws SQLException {

    }

    @Override
    protected String getCreateTable() {
        return null;
    }

    @Override
    protected String getTableName() {
        return null;
    }

    @Override
    protected String getUpdateQuery() {
        return null;
    }

    @Override
    protected String getInsertQuery() {
        return null;
    }

    @Override
    public void remove(Entity entity) {

    }
}