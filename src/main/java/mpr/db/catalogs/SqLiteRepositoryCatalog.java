package mpr.db.catalogs;

import mpr.db.*;
import mpr.db.repositories.*;
import mpr.db.repositories.retrievers.IEntityRetriever;
import mpr.db.unitofwork.UnitOfWork;

import java.sql.Connection;

/**
 * Created by galik on 21.01.2016.
 */
public class SqLiteRepositoryCatalog implements RepositoryCatalog{

    Connection connection;
    IEntityRetriever retriver;
    UnitOfWork uow;

    public SqLiteRepositoryCatalog(Connection connection) {
        this.connection = connection;
    }

    public SqLiteAddressRepository address() {
        return new SqLiteAddressRepository(connection, retriver, uow);
    }

    public SqLitePermissionRepository permission() {
        return new SqLitePermissionRepository(connection, retriver, uow);
    }

    public SqLiteRoleRepository role() {
        return new SqLiteRoleRepository(connection, retriver, uow);
    }

    public SqLitePersonRepository person() {
        return new SqLitePersonRepository(connection, retriver, uow);
    }

    public SqLiteRolePermissionRepository rolePermission() {
        return new SqLiteRolePermissionRepository(connection, retriver, uow);
    }

    public SqLitePersonRoleRepository personRole() {
        return new SqLitePersonRoleRepository(connection, retriver, uow);
    }

}
